var mutationObserver = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    console.log(mutation);

    if (mutation.addedNodes.length == 0) return;
    if (mutation.addedNodes[0].className != "bubble") return;

    // make sure, it's just one bubble
    if (1 <= document.querySelectorAll("div.bubble").length) {
      console.log("got a few new bubbles - something went wrong");
    } else if (document.querySelectorAll("div.bubble").length == 0) {
      console.log("no bubbles - something went wrong");
      return;
    }

    // read counter value
    var counter_before = document.querySelector("div#score").textContent;

    // pop a bubble (latest one)
    document.querySelector("div.bubble").click();

    //make sure, no bubbles available
    if (document.querySelectorAll("div.bubble").length != 0) {
      console.log("a few bubbles left - something went wrong");
    }

    // assure, that counter increased
    if (document.querySelector("div#score").textContent - 1 == counter_before) {
      console.log("successfuly popped a bubble");
    }
  });
});
//begin obervation
mutationObserver.observe(document.documentElement, {
  attributes: true,
  characterData: true,
  childList: true,
  subtree: true,
  attributeOldValue: true,
  characterDataOldValue: true,
});
