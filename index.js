//const API_URL ="https://api.github.com/repos/sahanr/street-fighter/contents/fighters.json";
//Get data from remote API using fetch
/*fetch(API_URL);*/

//Get promise
/*
const responsePromise = fetch(API_URL);
console.log(responsePromise);*/

//Add then method
/*
const responsePromise = fetch(API_URL);
responsePromise.then((response) => {
  console.log(response);
});*/

//Get response body
/*
const responsePromise = fetch(API_URL);
responsePromise.then((response) => {
  response.json();
});*/

//Get file content and parse it (Promise chain)
/*
fetch(API_URL)
  .then((response) => response.json())
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    console.log(fighters);
  });*/

//Check data types from response (Array and Object)
/*
fetch(API_URL)
  .then((response) => response.json())
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    console.log(names);
  });*/

//Display data using DOM API
/*const rootElement = document.getElementById("root");
rootElement.innerText = "Loading...";
fetch(API_URL)
  .then((response) => response.json())
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    rootElement.innerText = names;
  });*/

//onRejected handler (don`t forget to change URL )
/*const rootElement = document.getElementById("root");
rootElement.innerText = "Loading...";
fetch(API_URL)
  .then(
    (response) => {
      return response.json();
    },
    (error) => {
      console.warn(error);
    }
  )
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    rootElement.innerText = names;
  });*/

//catch method (don`t forget to change URL )
/*const rootElement = document.getElementById("root");
rootElement.innerText = "Loading...";
fetch(API_URL)
  .then((response) => response.json())
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    rootElement.innerText = names;
  })
  .catch((error) => {
    console.warn(error);
    root.innerText = "Failed to load data";
  });*/

//throw new Error (don`t forget to change URL )
/*const rootElement = document.getElementById("root");
rootElement.innerText = "Loading...";
fetch(API_URL)
  .then((response) => {
    if (!response.ok) {
      throw new Error("Failed load data");
    }
    return response.json();
  })
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    rootElement.innerText = names;
  })
  .catch((error) => {
    console.warn(error);
    root.innerText = "Failed to load data";
  });*/

//Promise.reject (don`t forget to change URL )
/*const rootElement = document.getElementById("root");
rootElement.innerText = "Loading...";
fetch(API_URL)
  .then((response) => {
    if (!response.ok) {
      return Promise.reject(new Error("Failed load data"));
    }
    return response.json();
  })
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    rootElement.innerText = names;
  })
  .catch((error) => {
    console.warn(error);
    root.innerText = "Failed to load data";
  });*/

//Add loading overlay and finally
/*const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");

fetch(API_URL)
  .then((response) => {
    if (!response.ok) {
      return Promise.reject(new Error("Failed load data"));
    }
    return response.json();
  })
  .then((file) => {
    const fighters = JSON.parse(atob(file.content));
    const names = fighters.map((it) => it.name).join("\n");
    rootElement.innerText = names;
  })
  .catch((error) => {
    console.warn(error);
    root.innerText = "Failed to load data";
  })
  .finally(() => {
    loadingElement.remove();
  });
*/
//Promise.resolve
/*const greetingPromise = Promise.resolve({ value: "Hello World" });
greetingPromise.then((res) => {
  console.log(res.value + "!");
});*/
/*
const promise = new Promise((resolve, reject) => {
  // some asynchronous code
  let isValid = true;

  if (isValid) {
    resolve("Success");
  } else {
    reject(Error("Error"));
  }
});*/

//Promise.race and Promise.all
/*function createPromise(message, ms) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(message), ms);
  });
}

const one = createPromise("first resolved", 2000);
const two = createPromise("second resolved", 3000);

const promiseRace = Promise.race([one, two]);
const promiseAll = Promise.all([one, two]);

promiseRace.then((res) => console.log(res)); // first resolved
promiseAll.then((res) => console.log(res)); // ['first resolved', 'second resolved']*/

//Add more Functions
/*const API_URL = "https://api.github.com/";
const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");

const startApp = function () {
  const endpoint = "repos/sahanr/street-fighter/contents/fighters.json";
  const fightersPromise = callApi(endpoint, "GET");

  fightersPromise.then((fighters) => {
    const fightersNames = getFightersNames(fighters);
    rootElement.innerText = fightersNames;
  });
};

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .then((file) => JSON.parse(atob(file.content)))
    .catch((error) => {
      console.warn(error);
      rootElement.innerText = "Failed to load data";
    })
    .finally(() => {
      loadingElement.remove();
    });
}

function getFightersNames(fighters) {
  const names = fighters.map((it) => it.name).join("\n");
  return names;
}

startApp();*/
//Async functions

/*const API_URL = "https://api.github.com/";
const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");

async function startApp() {
  const endpoint = "repos/sahanr/street-fighter/contents/fighters.json";
  const fighters = await callApi(endpoint, "GET");
  rootElement.innerText = getFightersNames(fighters);
}
function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .then((file) => JSON.parse(atob(file.content)))
    .catch((error) => {
      console.warn(error);
      rootElement.innerText = "Failed to load data";
    })
    .finally(() => {
      loadingElement.remove();
    });
}

function getFightersNames(fighters) {
  const names = fighters.map((it) => it.name).join("\n");
  return names;
}

startApp();*/

//Handle errors using try...catch statement
/*const API_URL = "https://api.github.com/";
const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");

async function startApp() {
  try {
    loadingElement.style.visibility = "visible";

    const endpoint = "repos/sahanr/street-fighter/contents/fighters.json";
    const fighters = await callApi(endpoint, "GET");

    rootElement.innerText = getFightersNames(fighters);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = "Failed to load data";
  } finally {
    loadingElement.style.visibility = "hidden";
  }
}

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .then((file) => JSON.parse(atob(file.content)))
    .catch((error) => {
      throw error;
    });
}

function getFightersNames(fighters) {
  const names = fighters.map((it) => it.name).join("\n");
  return names;
}
startApp();*/

//Add helper function for creating a DOM element
const API_URL = "https://api.github.com/";
const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");
const fightersDetailsMap = new Map();

/*async function startApp() {
  try {
    loadingElement.style.visibility = "visible";

    const endpoint = "repos/sahanr/street-fighter/contents/fighters.json";
    const fighters = await callApi(endpoint, "GET");
    const fightersElement = createFighters(fighters);
    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = "Failed to load data";
  } finally {
    loadingElement.style.visibility = "hidden";
  }
}*/

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )

    .catch((error) => {
      throw error;
    });
}
/*function createElement({ tagName, className = "", attributes = {} }) {
  const element = document.createElement(tagName);
  element.classList.add(className);
  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );

  return element;
}
function createName(name) {
  const nameElement = createElement({ tagName: "span", className: "name" });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-image",
    attributes,
  });

  return imgElement;
}
function createFighter(fighter) {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const element = createElement({ tagName: "div", className: "fighter" });
  element.addEventListener(
    "click",
    (event) => handleFighterClick(event, "fighter"),
    false
  );
  element.append(imageElement, nameElement);

  return element;
}
function createFighters(fighters) {
  const fighterElements = fighters.map((fighter) => createFighter(fighter));
  const element = createElement({ tagName: "div", className: "fighters" });

  element.append(...fighterElements);

  return element;
}

function handleFighterClick(event, fighter) {
  const { _id } = fighter;

  if (!fightersDetailsMap.has(_id)) {
    // send request here
    fightersDetailsMap.set(_id, fighter);
  }

  console.log(fightersDetailsMap.get(_id));
}

function getFightersNames(fighters) {
  const names = fighters.map((it) => it.name).join("\n");
  return names;
}
startApp();
*/
class FighterService {
  async getFighters() {
    try {
      const endpoint = "repos/sahanr/street-fighter/contents/fighters.json";
      const apiResult = await callApi(endpoint, "GET");

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
  async getFighterDetails(id) {
    try {
      const endpoint =
        "repos/sahanr/street-fighter/contents/details/fighter/fighters/${_id}.json";
      //const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, "GET");
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}
const fighterService = new FighterService();

class Fighter /*extends FighterService */ {
  async getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  async getHitPower() {
    var criticalHitChance = getRandom(1, 2);
    var power_hit = attack * criticalHitChance;
    console.log(power_hit);
  }
  async getBlockPower() {
    let dodgeChance = getRandom(1, 2);
    let power_block = attack * dodgeChance;
  }
}
class View {
  element;
  createElement({ tagName, className = "", attributes = {} }) {
    const element = document.createElement(tagName);
    element.classList.add(className);

    Object.keys(attributes).forEach((key) =>
      element.setAttribute(key, attributes[key])
    );

    return element;
  }
}

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: "div", className: "fighter" });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener(
      "click",
      (event) => handleClick(event, fighter),
      false
    );
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name",
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes,
    });

    return imgElement;
  }
}

class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map((fighter) => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters",
    });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    console.log("clicked");
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById("root");
  static loadingElement = document.getElementById("loading-overlay");

  async startApp() {
    try {
      App.loadingElement.style.visibility = "visible";

      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = "Failed to load data";
    } finally {
      App.loadingElement.style.visibility = "hidden";
    }
  }
}

new App();
