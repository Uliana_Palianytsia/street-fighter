/*2 */
console.log("Task2");
function superSort(str) {
  let ins = str.split(" ");
  let cleaned_original = {};
  for (var i in ins) {
    let word_original = ins[i];
    let word_clean = word_original
      .replace(/0/g, "")
      .replace(/1/g, "")
      .replace(/2/g, "")
      .replace(/3/g, "")
      .replace(/4/g, "")
      .replace(/5/g, "")
      .replace(/6/g, "")
      .replace(/7/g, "")
      .replace(/8/g, "")
      .replace(/9/g, "");
    cleaned_original[word_clean] = word_original;
  }
  let sorted_clean = Object.keys(cleaned_original).sort();
  let sorted = [];
  for (var i in sorted_clean) {
    sorted.push(cleaned_original[sorted_clean[i]]);
  }
  let outs = sorted.join(" ");
  console.log(outs);
}
let STR = "mic09ha1el 4b5en6 michelle be4atr3ice";
superSort(STR);

/*3*/
console.log("Task3");
function dateSort(str1, str2) {
  let Arr1 = str1.split(/[\s,./-]+/);
  let Arr2 = str2.split(/[\s,./-]+/);

  var day_1,
    day_2 = 0;
  var month_1,
    month_2 = 0;
  var year_1,
    year_2 = 0;

  if (Arr1[0].length == 4) {
    day_1 = Arr1[2];
    month_1 = Arr1[1];
    year_1 = Arr1[0];
  } else {
    day_1 = Arr1[0];
    month_1 = Arr1[1];
    year_1 = Arr1[2];
  }

  if (Arr2[0].length == 4) {
    day_2 = Arr2[2];
    month_2 = Arr2[1];
    year_2 = Arr2[0];
  } else {
    day_2 = Arr2[0];
    month_2 = Arr2[1];
    year_2 = Arr2[2];
  }

  if (day_1 == day_2 && month_1 == month_2 && year_1 == year_2) {
    console.log("True");
  } else if (day_1 != day_2 || month_1 != month_2 || year_1 != year_2) {
    console.log("False");
  }
}
dateSort("04/12/2030", "04.12.2030");
dateSort("2002,10,20", "01-02-2010");

/* #4 */
console.log("Task4");
function getNumber(Arr) {
  for (i = 0; i < Arr.length; i++) {
    if (Arr[i++] != Arr[i]) {
      console.log(Arr[i]);
    }
  }
}
let arr = new Array(1, 1, 1, 2, 1, 1);
let arr2 = new Array(0, 0, 0, 55, 0, 0);

getNumber(arr);
getNumber(arr2);
/* #5 */
console.log("Task5");
function getDiscount(number) {
  let discount = 0;
  let lastprice = 0;
  lastprice = price * number;
  if (number < 5) {
    return lastprice;
  }
  if (number >= 5 && number < 10) {
    lastprice = lastprice * 0.05;
  } else {
    lastprice = lastprice * 0.1;
  }
  return lastprice;
}
let price = 100;
console.log(getDiscount(2));

/* #6 */
console.log("Task6");
function getDaysForMonth(month) {
  if (month > 0 && month <= 12) {
    switch (month) {
      case 1:
        days = "31";
        break;
      case 2:
        days = "28";
        break;
      case 3:
        days = "31";
        break;
      case 4:
        days = "30";
        break;
      case 5:
        days = "31";
        break;
      case 6:
        days = "30";
        break;
      case 7:
        days = "31";
        break;
      case 8:
        days = "31";
        break;
      case 9:
        days = "30";
        break;
      case 10:
        days = "31";
        break;
      case 11:
        days = "30";
        break;
      case 12:
        days = "31";
        break;
    }
  } else {
    days = "Not a month";
  }
  return days;
}
console.log(getDaysForMonth(3));
